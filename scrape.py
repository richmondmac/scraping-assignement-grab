#!/usr/bin/env python
import csv
import json
import requests
from requests.api import options
from chromedriver_py import binary_path
from seleniumwire import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def interceptor(request):
    """
    Spoof the website so we don't have to deal with cloudfront errors
    Parameters
    ----------
    request : request
        selenium-wire request object to intercept

    """
    if request.url == 'https://food.grab.com/ph/en/restaurants':
        request.create_response(
            status_code=200,
            headers={'Content-Type': 'text/html'},
            body=open("captcha.html").read()
        )


def get_ph_restaurant(offset, driver, retries=5):
    """
    Get PH restaurants from grab API

    Parameters
    ----------
    offset : int
        api search page offset
    driver: driver
        Selenium driver object
    retries: int
        number of retries on failing api calls
    """
    if retries < 0:
        raise Exception("Maxmimum retries reached")
    driver.find_element(By.ID, "offsetmul").send_keys(offset)
    driver.find_element(By.ID, "btn").click()
    wait = WebDriverWait(driver, 10)
    i_d = str(offset) + '_token'
    wait.until(EC.presence_of_element_located((By.ID, i_d)))
    token_container = driver.find_element(By.ID, i_d)
    token = token_container.text.split()[-1]
    token_container.click()
    payload = {
        "latlng": "14.5995,120.9842",
        "keyword": "",
        "offset": offset,
        "pageSize": 32,
        "countryCode": "PH"}
    headers = {
        "x-country-code": "PH",
        "x-gfc-country": "PH",
        "x-grab-web-app-version": "~k5VPZk5KBtLKJOP7fLbR",
        "x-recaptcha-token": token
    }
    r = requests.post(
        'https://portal.grab.com/foodweb/v2/search',
        data=payload,
        headers=headers
    )
    if r.status_code != 200:
        print("retrying")
        return get_ph_restaurant(offset, driver, retries=retries-1)
    return json.loads(r.text)


def main():
    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("window-size=1400,2100")
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(executable_path=binary_path, options=chrome_options)
    driver.request_interceptor = interceptor

    driver.get("https://food.grab.com/ph/en/restaurants")
    offset = 0
    restaurants = []
    while True:
        results = get_ph_restaurant(offset, driver)\
            .get("searchResult").get("searchMerchants")
        if results:
            restaurants.extend(results or [])
        else:
            break
        offset += 8
    with open("output.csv", 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(("Name", "Lat", "Lng"))
        for r in restaurants:
            writer.writerow((
                r["address"]["name"],
                r["latlng"]["latitude"],
                r["latlng"]["longitude"]))


if __name__ == "__main__":
    main()
